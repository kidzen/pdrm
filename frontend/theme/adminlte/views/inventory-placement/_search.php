<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\InventoryPlacementSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-inventory-placement-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'asset_id')->textInput(['placeholder' => 'Asset']) ?>

    <?= $form->field($model, 'location')->textInput(['maxlength' => true, 'placeholder' => 'Location']) ?>

    <?= $form->field($model, 'approval')->textInput(['placeholder' => 'Approval']) ?>

    <?= $form->field($model, 'approved_by')->textInput(['placeholder' => 'Approved By']) ?>

    <?php /* echo $form->field($model, 'approved_at')->textInput(['placeholder' => 'Approved At']) */ ?>

    <?php /* echo $form->field($model, 'received_by')->textInput(['placeholder' => 'Received By']) */ ?>

    <?php /* echo $form->field($model, 'received_at')->textInput(['placeholder' => 'Received At']) */ ?>

    <?php /* echo $form->field($model, 'requested_by')->textInput(['placeholder' => 'Requested By']) */ ?>

    <?php /* echo $form->field($model, 'requested_at')->textInput(['placeholder' => 'Requested At']) */ ?>

    <?php /* echo $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Remark']) */ ?>

    <?php /* echo $form->field($model, 'status')->textInput(['placeholder' => 'Status']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
