<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InstOrderItem */

$this->title = 'Create Inst Order Item';
$this->params['breadcrumbs'][] = ['label' => 'Inst Order Item', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inst-order-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
