<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\InstOrderItem */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inst Order Item', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inst-order-item-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Inst Order Item'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'instOrder.id',
                'label' => 'Inst Order'
            ],
        [
                'attribute' => 'inventory.name',
                'label' => 'Inventory'
            ],
        'delivery_no',
        'received_date',
        'quantity_request',
        'quantity_received',
        'price_per_item',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
