<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Inventory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Inventory', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventory-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Inventory'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'category.name',
                'label' => 'Category'
            ],
        'name',
        'description',
        'type',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerInstOrderItem->totalCount){
    $gridColumnInstOrderItem = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'instOrder.id',
                'label' => 'Inst Order'
            ],
                'delivery_no',
        'received_date',
        'quantity_request',
        'quantity_received',
        'price_per_item',
        'remark',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInstOrderItem,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Inst Order Item'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnInstOrderItem
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerInventoryItem->totalCount){
    $gridColumnInventoryItem = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'parent_id',
        'serial_no',
        'manufacture_no',
        'cost',
        [
                'attribute' => 'warranty.id',
                'label' => 'Warranty'
            ],
        'type',
        'remark',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInventoryItem,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Inventory Item'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnInventoryItem
    ]);
}
?>
    </div>
</div>
