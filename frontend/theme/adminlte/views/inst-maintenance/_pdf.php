<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\InstMaintenance */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inst Maintenance', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inst-maintenance-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Inst Maintenance'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 't.id',
                'label' => 'T'
            ],
        'inst_no',
        [
                'attribute' => 'vendor.name',
                'label' => 'Vendor'
            ],
        [
                'attribute' => 'item.id',
                'label' => 'Item'
            ],
        'type',
        'total_cost',
        'approval',
        'approved_by',
        'approved_at',
        'received_by',
        'received_at',
        'requested_by',
        'requested_at',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
