<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Warranty */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Warranty', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="warranty-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Warranty'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'vendor.name',
                'label' => 'Vendor'
            ],
        'start_date',
        'end_date',
        'type',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerInventoryItem->totalCount){
    $gridColumnInventoryItem = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'inventory.name',
                'label' => 'Inventory'
            ],
        'parent_id',
        'serial_no',
        'manufacture_no',
        'cost',
                'type',
        'remark',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInventoryItem,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Inventory Item'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnInventoryItem
    ]);
}
?>
    </div>
</div>
