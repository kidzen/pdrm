<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Profile */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="user-form">

    <?= $form->field($Profile, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($Profile, 'fullname')->textInput(['maxlength' => true, 'placeholder' => 'Fullname']) ?>

    <?= $form->field($Profile, 'nickname')->textInput(['maxlength' => true, 'placeholder' => 'Nickname']) ?>

    <?= $form->field($Profile, 'position')->textInput(['maxlength' => true, 'placeholder' => 'Position']) ?>

    <?= $form->field($Profile, 'department')->textInput(['maxlength' => true, 'placeholder' => 'Department']) ?>

    <?= $form->field($Profile, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Remark']) ?>

    <?= $form->field($Profile, 'status')->textInput(['placeholder' => 'Status']) ?>

</div>
