<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Site */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Site', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Site'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerItemPlacement->totalCount){
    $gridColumnItemPlacement = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'item.id',
                'label' => 'Item'
            ],
        [
                'attribute' => 'itemMovement.id',
                'label' => 'Item Movement'
            ],
                'type',
        'remark',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerItemPlacement,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Item Placement'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnItemPlacement
    ]);
}
?>
    </div>
</div>
