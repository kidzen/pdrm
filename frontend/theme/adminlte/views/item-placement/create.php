<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ItemPlacement */

$this->title = 'Create Item Placement';
$this->params['breadcrumbs'][] = ['label' => 'Item Placement', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-placement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
