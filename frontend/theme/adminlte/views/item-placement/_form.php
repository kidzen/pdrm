<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ItemPlacement */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="item-placement-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>
    <div class="row">
    <?php // $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    <div class="col-md-4">
        <?= $form->field($model, 'item_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\common\models\InventoryItem::find()->orderBy('id')->asArray()->all(), 'id',['serial_no']),
            'options' => ['placeholder' => 'Choose Inventory item'],
            'pluginOptions' => [
            'allowClear' => true
            ],
            ]); ?>
        <?php
    // $form->field($model, 'item_movement_id')->widget(\kartik\widgets\Select2::classname(), [
    //     'data' => \yii\helpers\ArrayHelper::map(\common\models\ItemMovement::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
    //     'options' => ['placeholder' => 'Choose Item movement'],
    //     'pluginOptions' => [
    //         'allowClear' => true
    //     ],
    // ]);
        ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'site_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\common\models\Sites::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Choose Sites'],
            'pluginOptions' => [
            'allowClear' => true
            ],
            ]); ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'type')->textInput(['placeholder' => 'Type']) ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Remark']) ?>
    </div>
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
