<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\ItemPlacement */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Item Placement', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-placement-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Item Placement'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'item.id',
                'label' => 'Item'
            ],
        [
                'attribute' => 'itemMovement.id',
                'label' => 'Item Movement'
            ],
        [
                'attribute' => 'site.name',
                'label' => 'Site'
            ],
        'type',
        'approval',
        'approved_by',
        'approved_at',
        'received_by',
        'received_at',
        'requested_by',
        'requested_at',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
