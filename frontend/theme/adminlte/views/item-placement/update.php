<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ItemPlacement */

$this->title = 'Update Item Placement: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Item Placement', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="item-placement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
