<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\ItemMovement */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Item Movement', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-movement-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Item Movement'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'item.id',
                'label' => 'Item'
            ],
        'reff_id',
        'type',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerItemPlacement->totalCount){
    $gridColumnItemPlacement = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'item.id',
                'label' => 'Item'
            ],
                [
                'attribute' => 'site.name',
                'label' => 'Site'
            ],
        'type',
        'approval',
        'approved_by',
        'approved_at',
        'received_by',
        'received_at',
        'requested_by',
        'requested_at',
        'remark',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerItemPlacement,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Item Placement'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnItemPlacement
    ]);
}
?>
    </div>
</div>
