<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ItemMovement */

$this->title = 'Create Item Movement';
$this->params['breadcrumbs'][] = ['label' => 'Item Movement', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-movement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
