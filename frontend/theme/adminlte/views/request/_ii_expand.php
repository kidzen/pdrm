<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('InventoryItem'),
        'content' => $this->render('_ii_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Inst Maintenance'),
        'content' => $this->render('_ii_dataInstMaintenance', [
            'model' => $model,
            'row' => $model->instMaintenances,
        ]),
    ],
                [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Item Movement'),
        'content' => $this->render('_ii_dataItemMovement', [
            'model' => $model,
            'row' => $model->itemMovements,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Item Placement'),
        'content' => $this->render('_ii_dataItemPlacement', [
            'model' => $model,
            'row' => $model->itemPlacements,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
