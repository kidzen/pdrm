<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Vendor */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vendor', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendor-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Vendor'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'address',
        'phone',
        'fax',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerInstMaintenance->totalCount){
    $gridColumnInstMaintenance = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 't.id',
                'label' => 'T'
            ],
        'inst_no',
                [
                'attribute' => 'item.id',
                'label' => 'Item'
            ],
        'type',
        'total_cost',
        'approval',
        'approved_by',
        'approved_at',
        'received_by',
        'received_at',
        'requested_by',
        'requested_at',
        'remark',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInstMaintenance,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Instruction Maintenance'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnInstMaintenance
    ]);
}
?>
    </div>

    <div class="row">
<?php
if($providerInstOrder->totalCount){
    $gridColumnInstOrder = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 't.id',
                'label' => 'T'
            ],
        'inst_no',
                'type',
        'approval',
        'approved_by',
        'approved_at',
        'received_by',
        'received_at',
        'requested_by',
        'requested_at',
        'remark',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInstOrder,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Inst Order'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnInstOrder
    ]);
}
?>
    </div>
</div>
