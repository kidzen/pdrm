<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\InstMaintenance */

$this->title = 'Update Instruction Maintenance: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Instruction Maintenance', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="inst-maintenance-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
