<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AssetOrderList */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="asset-order-list-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'inst_no')->textInput(['maxlength' => true, 'placeholder' => 'Inst No']) ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true, 'placeholder' => 'Category']) ?>

    <?= $form->field($model, 'inventory')->textInput(['maxlength' => true, 'placeholder' => 'Inventory']) ?>

    <?= $form->field($model, 'quantity_request')->textInput(['placeholder' => 'Quantity Request']) ?>

    <?= $form->field($model, 'quantity_received')->textInput(['placeholder' => 'Quantity Received']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
