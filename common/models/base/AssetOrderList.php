<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "asset_order_list".
 *
 * @property integer $id
 * @property integer $inventory_id
 * @property integer $inst_order_id
 * @property integer $category_id
 * @property string $category
 * @property string $inventory
 * @property string $inst_no
 * @property integer $quantity_request
 * @property integer $quantity_received
 * @property double $price_per_item
 * @property integer $io_status
 * @property integer $ioi_status
 */
class AssetOrderList extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'inventory_id', 'inst_order_id', 'category_id', 'quantity_request', 'quantity_received', 'io_status', 'ioi_status'], 'integer'],
            [['price_per_item'], 'number'],
            [['category', 'inventory', 'inst_no'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'asset_order_list';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inventory_id' => 'Inventory ID',
            'inst_order_id' => 'Inst Order ID',
            'category_id' => 'Category ID',
            'category' => 'Category',
            'inventory' => 'Inventory',
            'inst_no' => 'Inst No',
            'quantity_request' => 'Quantity Request',
            'quantity_received' => 'Quantity Received',
            'price_per_item' => 'Price Per Item',
            'io_status' => 'Io Status',
            'ioi_status' => 'Ioi Status',
        ];
    }

/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AssetOrderListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\AssetOrderListQuery(get_called_class());
    }
}
