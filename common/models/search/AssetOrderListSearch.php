<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AssetOrderList;

/**
 * common\models\search\AssetOrderListSearch represents the model behind the search form about `common\models\AssetOrderList`.
 */
 class AssetOrderListSearch extends AssetOrderList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'inventory_id', 'inst_order_id', 'category_id', 'quantity_request', 'quantity_received', 'io_status', 'ioi_status'], 'integer'],
            [['category', 'inventory', 'inst_no'], 'safe'],
            [['price_per_item'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AssetOrderList::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'inventory_id' => $this->inventory_id,
            'inst_order_id' => $this->inst_order_id,
            'category_id' => $this->category_id,
            'quantity_request' => $this->quantity_request,
            'quantity_received' => $this->quantity_received,
            'price_per_item' => $this->price_per_item,
            'io_status' => $this->io_status,
            'ioi_status' => $this->ioi_status,
        ]);

        $query->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'inventory', $this->inventory])
            ->andFilterWhere(['like', 'inst_no', $this->inst_no]);

        return $dataProvider;
    }
}
