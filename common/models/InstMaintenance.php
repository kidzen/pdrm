<?php

namespace common\models;

use \common\models\base\InstMaintenance as BaseInstMaintenance;

/**
 * This is the model class for table "inst_maintenance".
 */
class InstMaintenance extends BaseInstMaintenance
{
    const TYPE_MAJOR = 0;
    const TYPE_MINOR = 1;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['t_id', 'vendor_id', 'item_id', 'type', 'total_cost', 'approval', 'approved_by', 'received_by', 'requested_by', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['approved_at', 'received_at', 'requested_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['inst_no', 'remark'], 'string', 'max' => 255],
            [['inst_no'], 'unique']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            't_id' => 'T ID',
            'inst_no' => 'Inst No',
            'vendor_id' => 'Vendor ID',
            'item_id' => 'Item ID',
            'type' => 'Type',
            'total_cost' => 'Total Cost',
            'approval' => 'Approval',
            'approved_by' => 'Approved By',
            'approved_at' => 'Approved At',
            'received_by' => 'Received By',
            'received_at' => 'Received At',
            'requested_by' => 'Requested By',
            'requested_at' => 'Requested At',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }

    public function generateInstNo() {
        $data = static::find()->max("CAST(SUBSTR(inst_no,25) AS UNSIGNED)");
        $bil = $data + 1;
        $bilPrefix = 'MZM/MAINTENANCE/'.date('Y/m/');
        return $bilPrefix.$bil;

    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->inst_no = $this->generateInstNo();
            $trans = new Transaction();
            $trans->type = $trans::TYPE_MAINTENANCE;
            $trans->remark = 'Asset Maintenance';
            $trans->save();
            return true;
        } else {
            return false;
        }
    }
}
