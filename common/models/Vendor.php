<?php

namespace common\models;

use \common\models\base\Vendor as BaseVendor;

/**
 * This is the model class for table "vendor".
 */
class Vendor extends BaseVendor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['phone', 'fax', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'address', 'remark'], 'string', 'max' => 255],
            [['name'], 'unique']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'address' => 'Address',
            'phone' => 'Phone',
            'fax' => 'Fax',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }
}
