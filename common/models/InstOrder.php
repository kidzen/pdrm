<?php

namespace common\models;

use \common\models\base\InstOrder as BaseInstOrder;

/**
 * This is the model class for table "inst_order".
 */
class InstOrder extends BaseInstOrder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['t_id', 'vendor_id', 'type', 'approval', 'approved_by', 'received_by', 'requested_by', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['approved_at', 'received_at', 'requested_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['inst_no', 'remark'], 'string', 'max' => 255],
            [['inst_no'], 'unique']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            't_id' => 'T ID',
            'inst_no' => 'Inst No',
            'vendor_id' => 'Vendor ID',
            'type' => 'Type',
            'approval' => 'Approval',
            'approved_by' => 'Approved By',
            'approved_at' => 'Approved At',
            'received_by' => 'Received By',
            'received_at' => 'Received At',
            'requested_by' => 'Requested By',
            'requested_at' => 'Requested At',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }

    public function generateInstNo() {
        $data = static::find()->max("CAST(SUBSTR(inst_no,19) AS UNSIGNED)");
        $bil = $data + 1;
        $bilPrefix = 'MZM/ASSET/'.date('Y/m/');
        return $bilPrefix.$bil;

    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->inst_no = $this->generateInstNo();
            $trans = new Transaction();
            $trans->type = $trans::TYPE_PURCHASING;
            $trans->remark = 'Asset Purchasing';
            $trans->save();
            return true;
        } else {
            return false;
        }
    }

}
